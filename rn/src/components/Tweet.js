import React from "react";
import { Image } from "react-native";
import { Card, Text } from "react-native-elements";
import moment from "moment";
import Icon from "react-native-vector-icons/Entypo";

import { Positive, Neutral, Negative } from "../assets";

const style = {
  username: {
    fontSize: 16,
    fontWeight: "bold",
    color: "black"
  },
  img: {
    width: 20,
    height: 20,
    position: "absolute",
    right: 10,
    top: 5
  },
  date: {
    textAlign: "right",
    opacity: 0.5
  }
};

const switchSentiment = sentiment => {
  switch (sentiment) {
    case "positive":
      return <Image style={style.img} source={Positive} />;
    case "neutral":
      return <Image style={style.img} source={Neutral} />;
    default:
      return <Image style={style.img} source={Negative} />;
  }
};

export default ({ username, classification, text, created_at }) => {
  return (
    <Card>
      {switchSentiment(classification)}
      <Text style={style.username}>@{username.toLowerCase() + "\n"}</Text>
      <Text>{text}</Text>
      <Text style={style.date}>
        {moment(created_at)
          .add(-3, "hour")
          .fromNow()}
      </Text>
    </Card>
  );
};
