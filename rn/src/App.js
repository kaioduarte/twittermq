import React, { Component } from "react";
import { Alert, View, ScrollView } from "react-native";
import {
  Card,
  Button,
  Icon,
  FormInput,
  FormLabel
} from "react-native-elements";
import axios from "axios";

import { Connection, Queue } from "react-native-rabbitmq";
import Tweet from "./components/Tweet";

const HOST = "192.168.0.102";

export default class App extends Component {
  state = { keywords: "", tweets: [] };

  componentDidMount() {
    const config = {
      host: HOST,
      port: 5672,
      username: "test",
      password: "test",
      virtualhost: "/"
    };

    let connection = new Connection(config);
    const pog = async () => {
      await connection.connect();
    };
    pog();

    let connected = false;
    let queue;

    connection.on("connected", async event => {
      queue = await new Queue(connection, {
        name: "classified_tweets",
        durable: false
      });

      // Receive one message when it arrives
      queue.on("message", data => {
        this.setState({
          tweets: [...this.state.tweets, JSON.parse(data.message)]
        });
      });
    });

    connection.on("error", event => {
      connected = false;
    });
  }

  onChange = e => {
    this.setState({ keywords: e.target.value });
  };

  sendKeywords = () => {
    const { keywords } = this.state;

    if (keywords === "") {
      Alert.alert("Type at least one keyword.");
      return;
    }

    axios
      .post(`http://${HOST}:5000/`, { keywords })
      .then(res => {
        if (res.status === 201) {
          this.setState({ keywords: "", tweets: [] });
        }
      })
      .catch(err => {
        Alert.alert("Fail to send request!");
      });
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <FormLabel>Keywords</FormLabel>
        <FormInput
          placeholder="Type the keywords separated by ','"
          onChangeText={keywords => this.setState({ keywords })}
          value={this.state.keywords}
        />
        <Button
          title="Search"
          backgroundColor="#2089dc"
          onPress={this.sendKeywords}
        />
        <ScrollView>
          {this.state.tweets.map((u, i) => <Tweet key={i} {...u} />)}
        </ScrollView>
      </View>
    );
  }
}
