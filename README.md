# Twitter Message Queue

## Bibliotecas

* [Tweepy](http://docs.tweepy.org)
* [Pika](http://pika.readthedocs.io)
* [TextBlob](http://textblob.readthedocs.io)
* [langdetect](https://github.com/Mimino666/langdetect)
* [RabbitMQ](https://www.rabbitmq.com)
* [Flask](http://flask.pocoo.org)
* [React Native](https://facebook.github.io/react-native/)
* [RabbitMQ RN](https://github.com/kegaretail/react-native-rabbitmq)

## Configurações

### RabbitMQ

* Iniciar servidor: `sudo rabbitmq-server -detached`
* Ativar gerenciador RabbitMQ: `sudo rabbitmq-plugins enable rabbitmq_management`
* Parar servidor: `sudo rabbitmqctl stop`
* Criar usuário com todas as permissões:

```
sudo rabbitmqctl add_user NOME SENHA
rabbitmqctl set_user_tags NOME administrator
rabbitmqctl set_permissions -p / NOME ".*" ".*" ".*"
```

* Criar arquivo de configuração, `rabbitmq.conf`, para permitir o acesso remoto às filas de mensagens com o seguinte conteúdo:

```
[
  {rabbit, [
    {tcp_listeners, [{"127.0.0.1", 5672},
    {"::1",       5672}]}
  ]}
].
```

* Acessar `http://localhost:15672` no navegador para visualizar o painel do administrador, o usuário e senha padrão são, respectivamente, `guest` e `guest`.

### React Native

* Baixar biblioteca `react-native-rabbitmq` e usar o comando `react-native link`.

* Configurações para conectar à fila, por exemplo:

```js
const config = {
  host: HOST,
  port: 5672,
  username: "test",
  password: "test",
  virtualhost: "/"
};
```
