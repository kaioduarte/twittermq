import re
import json
import pika
from textblob import TextBlob


try:
    # Connect to the RabbitMQ server
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()

    # Create a queue called <your_queue_name>
    channel.queue_declare(queue='raw_tweets')
    channel.queue_declare(queue='classified_tweets')
except Exception as err:
    print(str(err))
    exit(1)


def callback(ch, method, properties, body):
    print("[*] Consuming messages!")
    message = json.loads(body)
    message["classification"] = get_tweet_sentiment(
        clean_tweet(message["text"]))

    channel.basic_publish(exchange='',
                          routing_key='classified_tweets',
                          body=json.dumps(message))


def clean_tweet(tweet):
    '''
    Utility function to clean tweet text by removing links, special characters
    using simple regex statements.
    '''
    return ' '.join(
        re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)",
               " ",
               tweet).split())


def get_tweet_sentiment(tweet):
    '''
    Utility function to classify sentiment of passed tweet
    using textblob's sentiment method
    '''
    # create TextBlob object of passed tweet text
    analysis = TextBlob(clean_tweet(tweet))
    # set sentiment
    if analysis.sentiment.polarity > 0:
        return 'positive'
    elif analysis.sentiment.polarity == 0:
        return 'neutral'
    else:
        return 'negative'


if __name__ == "__main__":
    channel.basic_consume(callback,
                          queue='raw_tweets',
                          no_ack=True)
    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()
