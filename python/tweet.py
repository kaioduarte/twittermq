import re
import json
from time import sleep

import pika

from flask import Flask, request
from flask_cors import CORS

from langdetect import detect
from tweepy import OAuthHandler, Stream
from tweepy.streaming import StreamListener


try:
    # Connect to the RabbitMQ server
    credentials = pika.PlainCredentials('test', 'test')
    connection = pika.BlockingConnection(
        pika.ConnectionParameters('192.168.0.102', 5672, "/", credentials))
    channel = connection.channel()

    # Create a queue called <your_queue_name>
    channel.queue_declare(queue='raw_tweets')
except Exception as err:
    print(str(err))
    exit(1)


app = Flask(__name__)
CORS(app)

def create_stream(keywords: list):
    global mylistener, mystream

    mylistener = MyListener()
    mystream = Stream(auth, listener=mylistener)
    mystream.filter(track=keywords, async=True)


def clear_queue():
    global mystream

    channel.queue_purge(queue="raw_tweets")
    channel.queue_purge(queue="classified_tweets")

    if mystream:
        mystream.disconnect()


@app.route("/", methods=["POST"])
def set_keywords():
	print(request.data)
	res = json.loads(request.data)
	clear_queue()
	create_stream(res["keywords"].split(","))
	return "", 201


class MyListener(StreamListener):
    def on_data(self, dados):
        tweet = json.loads(dados)
        if "created_at" not in tweet:
            return

        text = tweet["text"]
        if text.startswith("RT"):
            return

        try:
            language = detect(text)
        except Exception:
            pass
        else:
            if language != "en":
                return

        obj = {
            "username": tweet["user"]["screen_name"],
            "created_at": re.sub("\+\d+ ", "", tweet["created_at"]),
            "text": tweet["text"]
        }

        channel.basic_publish(exchange='',
                              routing_key='raw_tweets',
                              body=json.dumps(obj))
        print(obj)
        return True


if __name__ == "__main__":
    consumer_key = "V7NpHutQ4OI7SnmSfVLvaTm3k"
    consumer_secret = "irUIEO5q7Anm8QSjcxytYcPYyWNZeMnbcK1Zat6Fap4CnhlJHX"

    access_token = "220017281-flQ8xX8EAwoiOTy9mkLN4UhmfW8VQIcfDWFnPzsA"
    access_secret = "EeT0E3R8JaXOiqr0K9MBvHsvymQIUgU95NDHJbsEAHaLv"

    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_secret)

    mylistener, mystream = None, None

    keywords = ['trump', 'apple']

    app.run(host="192.168.0.102", port=5000, debug=True)
